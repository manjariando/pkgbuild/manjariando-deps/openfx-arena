# Maintainer: Tércio Martins <echo dGVyY2lvd2VuZGVsQGdtYWlsLmNvbQo= | base64 -d>
# Contributor: Hugo Courtial <hugo [at] courtial [not colon] me>
# Contributor: Luca Weiss <luca (at) z3ntu (dot) xyz>

_lodepng_commit=8c6a9e30576f07bf470ad6f09458a2dcd7a6a84a
_SequenceParsing_commit=103c528347ebb2dd0ff5d79b5cee24bbcf938ce0
_tinydir_commit=64fb1d4376d7580aa1013fdbacddbbeba67bb085

pkgname=openfx-arena
pkgver=2.4.1
pkgrel=1
arch=('x86_64')
pkgdesc="Extra OpenFX plugins for Natron"
url="https://github.com/NatronGitHub/openfx-arena"
license=('GPL')
depends=('libcdr' 'libgl' 'libmagick' 'librsvg' 'libxt' 'libzip' 'opencolorio1' 'poppler-glib' 'sox' 'zstd')
makedepends=('jbigkit' 'openmp' 'pango')
conflicts=("${pkgname}-git")

_natron_ver="Natron-${pkgver}"
_pkgname="${pkgname}-${_natron_ver}"
_url=${url%/${pkgname}}

source=("${_pkgname}.tar.gz::${url}/archive/refs/tags/${_natron_ver}.tar.gz"
        "openfx-${_natron_ver}.tar.gz::${_url}/openfx/archive/refs/tags/${_natron_ver}.tar.gz"
        "openfx-io-${_natron_ver}.tar.gz::${_url}/openfx-io/archive/refs/tags/${_natron_ver}.tar.gz"
        "openfx-supportext-${_natron_ver}.tar.gz::${_url}/openfx-supportext/archive/${_natron_ver}.tar.gz"
        "lodepng-${_lodepng_commit}.tar.gz::https://github.com/lvandeve/lodepng/archive/${_lodepng_commit}.tar.gz"
        "SequenceParsing-${_SequenceParsing_commit}.tar.gz::${_url}/SequenceParsing/archive/${_SequenceParsing_commit}.tar.gz"
        "tinydir-${_tinydir_commit}.tar.gz::${_url}/tinydir/archive/${_tinydir_commit}.tar.gz")
sha512sums=('bf6095cc409470a1f7a18dd0865e4ebc324f09a58d25bdb5d2ecccc2c81ed6ccf029d7781f9af97446f252752eb24d1769cfc2b8892cb526b15523a08642d699'
            '0559401414508bdf14a785d1d43aeb0e40744a54b18ed33f9fca7bd577713ecc1841c1d4dbf14b7ad8ca5e413c1511668d16ee57c166341ab9ac45b87f2295f5'
            '564ef1d0f6b9ddb052eda862e68f10212fe3700d4d5c7e6d6907837fc6341ea925ef74bc1b48060e0528c4cc0ef534fb780e113bcfc823ef9af54e34250d54a4'
            'a8125170a3d3e9a4ee659be104063ff40781f5bf6e6c37e8d7ff7ff9500a4134e40c70bfa98a5013d93b8bd4bc163ca8505f460d00b81a77554b2307ebeb1072'
            '2e0abc063be45dc04a070656260e9a2b9fa1172433cdd7d4988f0afc11751ad28aa802350598ef0e2b27c2c011fd9d9f7ab7f267b0bfcdf28f9f708b888c4411'
            '733e2962f1d41cb50b967734f3f729a85d08366b7636bb59b05d0f6ff8d6f37b55d39158391aea4908eeb377620d125fba98c984ea69c0831a8fdd0c90e18b03'
            '5aedbfd41f2be73a5ee28b18dc6973bd5b7f15367aafce10ce3a7076cf0eb024ec62a6f693fe4e96defff445da2a3d95296a882e4b75a42cf5608c657d78913e')

prepare() {
    tar -xzf "lodepng-${_lodepng_commit}.tar.gz" --strip 1 \
        -C   "${_pkgname}/lodepng/"
    tar -xzf "openfx-${_natron_ver}.tar.gz" --strip 1 \
        -C   "${_pkgname}/OpenFX/"
    tar -xzf "openfx-supportext-${_natron_ver}.tar.gz" --strip 1 \
        -C   "${_pkgname}/SupportExt/"

    tar -xzf "openfx-io-${_natron_ver}.tar.gz" --strip 1 \
        -C   "${_pkgname}/OpenFX-IO/"
    tar -xzf "openfx-${_natron_ver}.tar.gz" --strip 1 \
        -C   "${_pkgname}/OpenFX-IO/openfx/"
    tar -xzf "openfx-supportext-${_natron_ver}.tar.gz" --strip 1 \
        -C   "${_pkgname}/OpenFX-IO/SupportExt/"

    tar -xzf "SequenceParsing-${_SequenceParsing_commit}.tar.gz" --strip 1 \
        -C   "${_pkgname}/OpenFX-IO/IOSupport/SequenceParsing/"
    tar -xzf "tinydir-${_tinydir_commit}.tar.gz" --strip 1 \
        -C   "${_pkgname}/OpenFX-IO/IOSupport/SequenceParsing/tinydir"

    # Change OpenColorIO library references to the version of "opencolorio1" package
    find "${_pkgname}/OpenFX-IO/IOSupport/" -name GenericOCIO.* \
            -exec sed -i 's/include <OpenColorIO/include <OpenColorIO1/' {} \;

    # Solve a problem in the linking of the "Extra" plugins,
    # caused by a misconfiguration of pkgconfig in the "libzip" package
    sed -i '/ZIP_LINKFLAGS/ s|\$.*libs.*|-lbz2 -llzma -lzstd -lgnutls -lnettle -lz|' \
            "${_pkgname}/Makefile.master"
}

build() {
    cd "${srcdir}/${_pkgname}"
    make CONFIG=release
}

package() {
    cd "${srcdir}/${_pkgname}"
    mkdir -p "${pkgdir}/usr/OFX/Plugins"
    make install PLUGINPATH="${pkgdir}/usr/OFX/Plugins" \
                CONFIG=release
}
